<?php

/**
 * @file
 * Rules integration for the Customizable Products Pricing - Entity Reference module.
 */

/**
 * Implements hook_rules_event_info().
 *
 * Defines the event used to calculate the price modification of an entity
 * selected on a custom product's line item.
 */
function commerce_custom_product_pricing_er_rules_event_info() {
  $items = array();

  $items['commerce_custom_product_pricing_er_calculate_option_price'] = array(
    'label' => t('Calculating the price of a line item Entity Reference option'),
    'group' => t('Commerce Customizable Products'),
    'variables' => array(
      'option_price' => array(
        'label' => t('Option Price'),
        'type' => 'commerce_price',
      ),
      'option_entity' => array(
        'label' => t('Option Entity'),
        'type' => 'entity',
        'skip save' => TRUE,
      ),
      'commerce_line_item' => array(
        'label' => t('Product line item'),
        'type' => 'commerce_line_item',
        'skip save' => TRUE,
      ),
      'field_name' => array(
        'label' => t('Field name'),
        'type' => 'token',
      ),
    ),
  );

  return $items;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_custom_product_pricing_er_rules_action_info() {
  $actions = array();

  $actions['commerce_price_currency_convert'] = array(
    'label' => t("Convert a price to a different currency"),
    'parameter' => array(
      'price' => array(
        'label' => t('Price'),
        'type' => 'commerce_price',
        'default mode' => 'selector',
      ),
      'currency_code' => array(
        'type' => 'text',
        'label' => t('Currency'),
        'options list' => 'commerce_currency_get_code',
      ),
    ),
    'provides' => array(
      'converted_price' => array(
        'type' => 'commerce_price',
        'label' => t('Converted price'),
      ),
    ),
    'group' => t('Commerce Price'),
    'callbacks' => array(
      'execute' => 'commerce_custom_product_pricing_er_price_currency_convert',
    ),
  );

  return $actions;
}

/**
 * Rules action: convert the price to a different currency.
 */
function commerce_custom_product_pricing_er_price_currency_convert($price, $currency_code) {
  $converted_price = commerce_price_field_data_auto_creation();

  // Only convert non-NULL amounts since NULL amounts do not have a currency.
  if (empty($price)) {
    return;
  }

  $converted_price['amount'] = commerce_currency_convert($price['amount'], $price['currency_code'], $currency_code);
  $converted_price['currency_code'] = $currency_code;

  // Convert the currency code of the price's components.
  if (!empty($price['data']['components'])) {
    foreach ($price['data']['components'] as $key => &$component) {
      $component['price']['amount'] = commerce_currency_convert($component['price']['amount'], $component['price']['currency_code'], $currency_code);
      $component['price']['currency_code'] = $currency_code;
    }

    $converted_price['data'] = $price['data'];
  }

  return array('converted_price' => $converted_price);
}
