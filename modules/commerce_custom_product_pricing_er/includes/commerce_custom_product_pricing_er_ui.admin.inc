<?php

/**
 * @file
 * Administrative page callbacks for the Customizable Products Pricing - Entity Reference UI module.
 */


/**
 * Builds the product sell price calculation Rules Overview page.
 */
function commerce_custom_product_pricing_er_ui_option_price_rules() {
  RulesPluginUI::$basePath = 'admin/commerce/config/custom-product-pricing-entityreference/rules';
  $options = array('show plugin' => FALSE);

  $content['enabled']['title']['#markup'] = '<h3>' . t('Enabled Entity Reference option price calculation rules') . '</h3>';

  $conditions = array('event' => 'commerce_custom_product_pricing_er_calculate_option_price', 'plugin' => 'reaction rule', 'active' => TRUE);
  $content['enabled']['rules'] = RulesPluginUI::overviewTable($conditions, $options);
  $content['enabled']['rules']['#empty'] = t('There are no active option price calculation rules.');

  $content['disabled']['title']['#markup'] = '<h3>' . t('Disabled Entity Reference option price calculation rules') . '</h3>';

  $conditions['active'] = FALSE;
  $content['disabled']['rules'] = RulesPluginUI::overviewTable($conditions, $options);
  $content['disabled']['rules']['#empty'] = t('There are no disabled option price calculation rules.');

  return $content;
}
