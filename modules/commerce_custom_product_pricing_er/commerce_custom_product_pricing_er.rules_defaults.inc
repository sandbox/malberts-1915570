<?php
/**
 * @file
 * commerce_custom_product_pricing_er.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_custom_product_pricing_er_default_rules_configuration() {
  $items = array();

  // Example rule to calculate a Product entity option's value from its
  // commerce_price field.
  $items['commerce_custom_product_pricing_er_product_option'] = entity_import('rules_config', '{ "commerce_custom_product_pricing_er_product_option" : {
      "LABEL" : "Commerce Product Option Price",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules", "commerce_custom_product_pricing_er" ],
      "ON" : [ "commerce_custom_product_pricing_er_calculate_option_price" ],
      "IF" : [
        { "entity_is_of_type" : { "entity" : [ "option-entity" ], "type" : "commerce_product" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "option-price:amount" ],
            "value" : [ "option-entity:commerce-price:amount" ]
          }
        },
        { "data_set" : {
            "data" : [ "option-price:currency-code" ],
            "value" : [ "option-entity:commerce-price:currency-code" ]
          }
        },
        { "data_set" : {
            "data" : [ "option-price:data" ],
            "value" : [ "option-entity:commerce-price:data" ]
          }
        }
      ]
    }
  }');

  // Same as above, but converts to the current currency first.
  $items['commerce_custom_product_pricing_er_product_option_multicurrency'] = entity_import('rules_config', '{ "commerce_custom_product_pricing_er_product_option_price_multicurrency" : {
      "LABEL" : "Commerce Product Option Price (Multicurrency)",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules", "commerce_custom_product_pricing_er" ],
      "ON" : [ "commerce_custom_product_pricing_er_calculate_option_price" ],
      "IF" : [
        { "entity_is_of_type" : { "entity" : [ "option-entity" ], "type" : "commerce_product" } }
      ],
      "DO" : [
        { "commerce_price_currency_convert" : {
            "USING" : {
              "price" : [ "option-entity:commerce-price" ],
              "currency_code" : [ "site:commerce-currency" ]
            },
            "PROVIDE" : { "converted_price" : { "converted_price" : "Converted price" } }
          }
        },
        { "data_set" : {
            "data" : [ "option-price:amount" ],
            "value" : [ "converted-price:amount" ]
          }
        },
        { "data_set" : {
            "data" : [ "option-price:currency-code" ],
            "value" : [ "converted-price:currency-code" ]
          }
        },
        { "data_set" : { "data" : [ "option-price:data" ], "value" : [ "converted-price:data" ] } }
      ]
    }
  }');

  return $items;
}
