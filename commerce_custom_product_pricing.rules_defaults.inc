<?php
/**
 * @file
 * commerce_custom_product_pricing.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_custom_product_pricing_default_rules_configuration() {
  $items = array();

  // Default rule for adding the total price of all the selected options on a
  // line item to the line item's unit price.
  $items['commerce_custom_product_pricing_selected_options_price'] = entity_import('rules_config', '{ "commerce_custom_product_pricing_selected_options_price" : {
      "LABEL" : "Add selected options total price",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-1",
      "REQUIRES" : [ "commerce_line_item", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "DO" : [
        { "commerce_line_item_unit_price_add" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "commerce-line-item:selected-options-price:amount" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');

  return $items;
}
