<?php

/**
 * @file
 * Administrative page callbacks for the Customizable Products Pricing - Entity Reference UI module.
 */


/**
 * Builds the product sell price calculation Rules Overview page.
 */
function commerce_custom_product_pricing_ui_settings_form($form, &$form_state) {
  $form['commerce_custom_product_pricing_symbol_type'] = array(
    '#type' => 'select',
    '#title' => t('Relative price symbols'),
    '#description' => t('The symbols to use when displaying the relative price of an option in a field.'),
    '#options' => array(
      'text' => t('Add') . ' / ' . t('Subtract'),
      'sign' => '+ / -',
    ),
    '#default_value' => variable_get('commerce_custom_product_pricing_symbol_type', 'text'),
  );
  
  $form['commerce_custom_product_pricing_price_wrapper_type'] = array(
    '#type' => 'select',
    '#title' => t('Price wrapper'),
    '#description' => t('The symbols to use around the price on the Add to Cart form.'),
    '#options' => array(
      'brackets' => '[ ]',
      'parentheses' => '( )',
    ),
    '#default_value' => variable_get('commerce_custom_product_pricing_price_wrapper', 'brackets'),
  );
  
  return system_settings_form($form);
}
