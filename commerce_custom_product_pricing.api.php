<?php

/**
 * @file
 * Hooks provided by the Customizable Product Pricing module.
 */


/**
 * Allows modules to alter the price for a selected option on a customizable product
 * line item.
 * 
 * @param $price
 *   The price array.
 * @param $option
 *   The option's key.
 * @param $context
 *   A keyed array containing:
 *   - line_item: the current line item object on the Add to Cart form.
 *   - instance: the field instance's settings array.
 */
function hook_commerce_custom_product_pricing_calculate_option_price_alter($price, $option, $context) {
  // No example.
}
